
#import "UICountdownCounterView.h"

@implementation UICountdownCounterView {
    UIImageView *_backgroundImageView;
    UIImageView *_foregroundImageView;
    CAShapeLayer *circleLayer;

    double strokeWidth;
}


- (id)initWithBackgroundImage:(UIImage *)backgroundImage foregroundImage:(UIImage *)foregroundImage {
    self = [super init];
    if (self) {

        _backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
        _foregroundImageView = [[UIImageView alloc] initWithImage:foregroundImage];

        self.frame = CGRectMake(0, 0, backgroundImage.size.width, backgroundImage.size.height);

        strokeWidth = 12.5f;

        const float backgroundRadius = backgroundImage.size.width / 2;
        const float foregroundRadius = foregroundImage.size.width / 2;

        CGPoint pointMake = CGPointMake(CGRectGetMidX(self.frame) - backgroundRadius,
                CGRectGetMidY(self.frame) - backgroundRadius);

        CGPoint pointMakeGrey = CGPointMake(CGRectGetMidX(self.frame) - foregroundRadius,
                CGRectGetMidY(self.frame) - foregroundRadius);


        CGRect rect = _backgroundImageView.frame;
        rect.origin = pointMake;
        _backgroundImageView.frame = rect;
        [self addSubview:_backgroundImageView];

        CGRect gradientRect = _foregroundImageView.frame;
        gradientRect.origin = pointMakeGrey;
        _foregroundImageView.frame = gradientRect;
        [self addSubview:_foregroundImageView];


        circleLayer = [CAShapeLayer layer];
        circleLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0 * backgroundRadius - strokeWidth, 2.0 * backgroundRadius - strokeWidth)
                                                      cornerRadius:backgroundRadius].CGPath;
        circleLayer.position = CGPointMake(pointMake.x + strokeWidth/2, pointMake.y + strokeWidth/2);
        circleLayer.fillColor = [UIColor clearColor].CGColor;
        circleLayer.strokeColor = [UIColor whiteColor].CGColor;
        circleLayer.lineWidth = strokeWidth + 0.5f;
        circleLayer.transform = CATransform3DTranslate(CATransform3DMakeRotation(3.1412,0,0,1), -(2.0 * backgroundRadius - strokeWidth),-(2.0 * backgroundRadius - strokeWidth), 0);
        [self.layer addSublayer:circleLayer];

    }
    return self;
}

- (void)startAnimation:(double)duration repeatCount:(double)repeatCount {
    [circleLayer addAnimation:[self getAnimation:duration repeatCount:repeatCount] forKey:@"drawCircleAnimation"];
}

- (void)stopAnimation {
    [circleLayer removeAllAnimations];
}

- (CABasicAnimation *)getAnimation:(double)duration repeatCount:(double)repeatCount {
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration = duration;
    drawAnimation.repeatCount = repeatCount;

    drawAnimation.fromValue = [NSNumber numberWithFloat:1];
    drawAnimation.toValue = [NSNumber numberWithFloat:0];

    drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    return drawAnimation;
}

+ (Class) layerClass
{
    return [CAShapeLayer class];
}

- (void)layoutSubviews {
    [super layoutSubviews];

}

- (CGSize)intrinsicContentSize {
    return self.frame.size;
}


@end
