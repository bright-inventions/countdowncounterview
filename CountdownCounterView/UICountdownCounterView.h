#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UICountdownCounterView : UIView

@property(nonatomic) int percent;
@property (strong, nonatomic) UIImage *background;

- (id)initWithBackgroundImage:(UIImage *)backgroundImage foregroundImage:(UIImage *)foregroundImage;

- (void)startAnimation:(double)duration repeatCount:(double)repeatCount;

- (void)stopAnimation;
@end
