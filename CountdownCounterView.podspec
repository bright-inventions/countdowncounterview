Pod::Spec.new do |s|  
  s.name         = "CountdownCounterView"
  s.version      = "0.0.1"
  s.summary      = "Circural progress bar with bitmap background"


  s.source       = { :git => "git@bitbucket.org:mgamer/countdowncounterview.git", :tag => "0.0.1" }
  s.homepage     = "https://bitbucket.org/mgamer/countdowncounterview"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { "Bright Inventions" => "mateusz.klimek@brightinventions.pl" }
  s.platform     = :ios, '6.0'
  s.source_files  = 'CountdownCounterView', 'CountdownCounterView/*.{h,m}'
  s.public_header_files = 'CountdownCounterView/**/*.h'
  s.framework    = 'SystemConfiguration'
  s.requires_arc = true

end  